package main

import (
	"fmt"
	"io"
	"playground/internal/playground/ui"
)

func main() {
	for {
		ui.ShowSolvers()
		if err := ui.ChooseSolver(); err != nil {
			if err == io.EOF {
				fmt.Printf("Got EOF. Good Buy!\n")
				break
			}
			fmt.Printf("Can't choose solver: %v\n", err)
			continue
		}

		fmt.Printf("OK, work with the choosen solver %s\n", ui.GetSolverName())
		ui.SolverLoop()
	}
}
