package main

import (
	"fmt"
	"playground/internal/pkg/domain"
	"playground/internal/pkg/fib1"
	"playground/internal/pkg/primes"
	"playground/test"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/gookit/color"
)

func main() {
	// s := fib1.New()
	// res, _ := s.Run([]string{"20"})
	// fmt.Println(res)

	// tester(str.New(), test.ShowInput, test.ShowResult, test.ShowExpected)
	// tester(fib2.New())
	tester(primes.New())
	tester(fib1.New())
	// tester(king.New())
	// tester(knight.New(), test.ShowInput, test.ShowResult)
}

func tester(solver domain.Solver, opts ...test.TesterOption) {
	showResult := false
	showInput := false
	rawDuration := false
	noCheckResult := false
	showExpected := false

	for _, opt := range opts {
		switch opt {
		case test.ShowResult:
			showResult = true
		case test.ShowInput:
			showInput = true
		case test.RawDuration:
			rawDuration = true
		case test.NoCheckResult:
			noCheckResult = true
		case test.ShowExpected:
			showExpected = true
		}
	}

	fmt.Printf("\ntesting %s\n", solver.Name())
	for _, tt := range test.GetTestCasesFromDir(solver.Dir()) {
		start := time.Now()
		got, err := solver.Run(tt.In)
		elapsed := time.Since(start)
		if err != nil {
			fmt.Printf("can't solve: %v\n", err)
			break
		}

		var elapsedStr string
		if rawDuration {
			elapsedStr = elapsed.String()
		} else {
			elapsedStr = fmt.Sprintf("%fms", float64(elapsed)/float64(time.Millisecond))
		}

		fmt.Printf("test %s took %s\n", tt.Name, elapsedStr)

		if showInput {
			fmt.Printf("input: %+v\n", tt.In)
		}
		if showResult {
			fmt.Printf("result: %+v\n", got)
		}

		if showExpected {
			msg := fmt.Sprintf("expected: %+v\n", tt.Out)

			if !cmp.Equal(tt.Out, got) {
				color.Red.Print(msg)
			} else {
				fmt.Print(msg)
			}
		}

		if !noCheckResult && !cmp.Equal(tt.Out, got) {
			color.Red.Printf("Test %s failed.\ninput: %+v\nexpected: %+v\ngot: %+v\n", tt.Name, tt.In, tt.Out, got)
			break
		}

	}

	fmt.Printf("done.\n")

}
