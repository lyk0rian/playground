package ui

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"playground/internal/pkg/domain"
	"playground/internal/pkg/luckynumbers"
	"playground/internal/pkg/str"
	"strings"
)

var solvers = []domain.Solver{}
var solversMap = map[string]domain.Solver{}
var currentSolver domain.Solver

var reader = bufio.NewReader(os.Stdin)

func init() {
	solvers = []domain.Solver{
		str.New(),
		luckynumbers.New(),
	}

	for i := range solvers {
		num := fmt.Sprintf("%d", i+1)
		solversMap[num] = solvers[i]
	}
}

func ShowSolvers() {
	fmt.Printf("Available Solvers:\n")
	for i, solver := range solvers {
		fmt.Printf("%d. %s\n", i+1, solver.Name())
	}
}

func ChooseSolver() error {
	fmt.Printf("Please choose the solver you want to work with\n")
	text, err := GetUserInput()
	if err != nil {
		return err
	}
	var ok bool
	currentSolver, ok = solversMap[text]
	if !ok {
		return fmt.Errorf("solver '%s' is not registered", text)
	}

	return nil
}

func GetSolverName() string {
	if currentSolver == nil {
		return ""
	}

	return currentSolver.Name()
}

func GetUserInput() (line string, err error) {
	tpl := ""
	name := GetSolverName()
	if len(name) > 0 {
		tpl = fmt.Sprintf("[%s]", name)
	}
	fmt.Printf("%s-> ", tpl)
	line, err = reader.ReadString('\n')
	if err != nil {
		return
	}
	line = strings.Trim(line, " \t\r\n")
	return
}

func SolverLoop() {
	if currentSolver == nil {
		panic("Solver is not set")
	}
	for {
		line, err := GetUserInput()
		if err == io.EOF {
			fmt.Printf("got EOF\n")
			currentSolver = nil
			break
		}
		if err != nil {
			fmt.Printf("got error: %v\n", err)
			continue
		}

		result, err := currentSolver.Run([]string{line})
		if err != nil {
			fmt.Printf("got error: %v\n", err)
			continue
		}

		fmt.Printf("result:\n%s\n", result)
	}
}
