package domain

type Solver interface {
	Run([]string) ([]string, error)
	Name() string
	Dir() string
}

func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}
