package luckynumbers

import (
	"fmt"
	"playground/test"
	"strconv"
	"strings"
)

type LuckyNumbers struct {
}

func New() *LuckyNumbers {
	return &LuckyNumbers{}
}

func (ln *LuckyNumbers) Name() string {
	return "Lucky Numbers"
}

func (ln *LuckyNumbers) Dir() string {
	return test.GetModuleDir(1)
}

func (ln *LuckyNumbers) Run(params []string) (result []string, err error) {
	n, err := parseParams(params)
	if err != nil {
		return
	}

	vector := makeSumVector(n)
	sum := uint64(0)

	for _, cases := range vector {
		sum += cases * cases
	}

	return []string{fmt.Sprintf("%d", sum)}, nil
}

func parseParams(params []string) (result int, err error) {
	if len(params) < 1 {
		err = fmt.Errorf("invalid params")
		return
	}

	parsed, err := strconv.ParseInt(strings.Trim(params[0], "\r\n"), 10, 64)
	if err != nil {
		return
	}

	if parsed < 1 {
		err = fmt.Errorf("n is too small, it should be greater than 0")
		return
	}
	if parsed > 10 {
		err = fmt.Errorf("n is too big, it should be 10 or less")
		return
	}

	return int(parsed), nil
}

func makeSumVector(n int) []uint64 {
	if n == 1 {
		return []uint64{1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
	}

	prevVector := makeSumVector(n - 1)
	result := make([]uint64, (n+1)*9+1)
	for i := 0; i < 10; i++ {
		for j := 0; j < len(prevVector); j++ {
			result[i+j] += prevVector[j]
		}
	}

	return result
}
