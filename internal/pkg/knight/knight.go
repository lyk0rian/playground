package knight

import (
	"fmt"
	"playground/test"
	"strconv"
)

type Solver struct {
	primes []int64
}

func New() *Solver {
	return &Solver{}
}

func (s *Solver) Name() string {
	return "Конь - BITS"
}

func (s *Solver) Dir() string {
	return test.GetModuleDir(1)
}

func (s *Solver) Run(params []string) ([]string, error) {
	cellNo, err := s.parseParams(params)
	if err != nil {
		return nil, err
	}

	mask := getMovesMask(uint64(cellNo))
	moves := popCnt(mask)

	return []string{strconv.FormatUint(uint64(moves), 10), strconv.FormatUint(mask, 10)}, nil
}

func (s *Solver) parseParams(params []string) (n int64, err error) {
	if len(params) < 1 {
		err = fmt.Errorf("incorrect params")
		return
	}

	n, err = strconv.ParseInt(params[0], 10, 64)
	if err != nil {
		return
	}

	if n < 0 || n > 64 {
		err = fmt.Errorf("incorrect params")
		return
	}
	return
}

func getMovesMask(cellNo uint64) (mask uint64) {
	knight := uint64(1) << uint64(cellNo)
	noA := uint64(0xfefefefefefefefe)
	noAB := uint64(0xfcfcfcfcfcfcfcfc)
	noH := uint64(0x7f7f7f7f7f7f7f7f)
	noGH := uint64(0x3f3f3f3f3f3f3f3f)

	mask = noGH&(knight<<6|knight>>10) |
		noH&(knight<<15|knight>>17) |
		noA&(knight<<17|knight>>15) |
		noAB&(knight<<10|knight>>6)

	return
}

func popCnt(val uint64) int {
	bits := 0
	for val != 0 {
		if val&1 == 1 {
			bits++
		}

		val >>= 1
	}

	return bits
}
