package fib2

import (
	"fmt"
	"math/big"
	"playground/test"
	"strconv"
)

type Fib struct {
}

func New() *Fib {
	return &Fib{}
}

func (f *Fib) Name() string {
	return "Итеративный O(N) алгоритм поиска чисел Фибоначчи"
}

func (f *Fib) Dir() string {
	return test.GetModuleDir(1)
}

func (f *Fib) Run(params []string) (string, error) {

	n, err := f.parseParams(params)
	if err != nil {
		return "", err
	}

	result := f.fib(uint64(n))

	return fmt.Sprintf("%d", result), nil
}

func (f *Fib) fib(n uint64) (result *big.Int) {
	if n < 1 {
		return big.NewInt(0)
	}

	cache := [2]*big.Int{big.NewInt(1), big.NewInt(1)}

	for i := uint64(2); i < n; i++ {
		cache[i%2] = new(big.Int).Add(cache[0], cache[1])
		// fmt.Printf("i: %d, cache: %+v, %d\n", i, cache, cache[i%2])
	}

	return cache[(n-1)%2]
}

func (f *Fib) parseParams(params []string) (n int64, err error) {
	if len(params) < 1 {
		err = fmt.Errorf("incorrect params")
		return
	}

	n, err = strconv.ParseInt(params[0], 10, 64)
	if err != nil {
		return
	}

	if n < 0 {
		err = fmt.Errorf("incorrect params")
		return
	}
	return
}
