package primes

import (
	"fmt"
	"math"
	"playground/test"
	"strconv"
)

type Solver struct {
	primes []int64
}

func New() *Solver {
	return &Solver{}
}

func (s *Solver) Name() string {
	return "Количество простых чисел от 1 до N, оптимизированный перебор делителей"
}

func (s *Solver) Dir() string {
	return test.GetModuleDir(1)
}

func (s *Solver) Run(params []string) ([]string, error) {

	n, err := s.parseParams(params)
	if err != nil {
		return nil, err
	}

	result := s.countPrimes(n)

	return []string{fmt.Sprintf("%d", result)}, nil
}

func (s *Solver) countPrimes(n int64) (result int64) {
	if n < 2 {
		return 0
	}

	count := int64(1)
	s.primes = []int64{2}

	for i := int64(3); i <= n; i += 2 {
		if s.isPrime(i) {
			count++
			s.primes = append(s.primes, i)
		}
	}

	return count
}

func (s *Solver) isPrime(n int64) (result bool) {
	if n == 2 {
		return true
	}

	if n%2 == 0 {
		return false
	}

	max := int64(math.Sqrt(float64(n)))
	for _, prime := range s.primes {
		if prime > max {
			break
		}

		if n%prime == 0 {
			return false
		}
	}

	return true
}

func (s *Solver) parseParams(params []string) (n int64, err error) {
	if len(params) < 1 {
		err = fmt.Errorf("incorrect params")
		return
	}

	n, err = strconv.ParseInt(params[0], 10, 64)
	if err != nil {
		return
	}

	if n < 1 {
		err = fmt.Errorf("incorrect params")
		return
	}
	return
}
