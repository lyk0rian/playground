package power1

import (
	"fmt"
	"playground/test"
	"strconv"
)

type Power struct {
}

func New() *Power {
	return &Power{}
}

func (s *Power) Name() string {
	return "Возведение в степень через домножение O(N/2+LogN) = O(N)"
}

func (ln *Power) Dir() string {
	return test.GetModuleDir(1)
}

func (s *Power) Run(params []string) ([]string, error) {

	a, n, err := s.parseParams(params)
	if err != nil {
		return nil, err
	}

	if n == 0 {
		return []string{"1"}, nil
	}

	result := a
	i := int64(1)
	for ; i <= int64(n/2); i *= 2 {
		result *= result
	}

	for ; i < n; i++ {
		result = result * a
	}

	return []string{strconv.FormatFloat(result, 'f', 11, 64)}, nil
}

func (s *Power) parseParams(params []string) (a float64, n int64, err error) {
	if len(params) < 2 {
		err = fmt.Errorf("incorrect params")
		return
	}

	a, err = strconv.ParseFloat(params[0], 64)
	if err != nil {
		return
	}

	n, err = strconv.ParseInt(params[1], 10, 64)

	return
}
