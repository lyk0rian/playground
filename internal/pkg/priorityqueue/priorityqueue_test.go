package priorityqueue

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestEmptyQueue(t *testing.T) {
	q := New()
	_, err := q.Dequeue()
	require.Error(t, err)
}

type priorityItem struct {
	priority int
	value    int
}

func TestPriurityQueue(t *testing.T) {
	tests := []struct {
		in  []priorityItem
		out []interface{}
	}{
		{
			in: []priorityItem{
				{priority: 4, value: 1},
				{priority: 4, value: 3},
				{priority: 4, value: 5},
				{priority: 3, value: 2},
				{priority: 1, value: 8},
			},
			out: []interface{}{8, 2, 1, 3, 5},
		},
		{
			in: []priorityItem{
				{priority: 3, value: 5},
				{priority: 2, value: 4},
				{priority: 1, value: 2},
				{priority: 0, value: 1},
				{priority: 1, value: 3},
			},
			out: []interface{}{1, 2, 3, 4, 5},
		},
		{
			in: []priorityItem{
				{priority: 5, value: 1},
				{priority: 5, value: 2},
				{priority: 5, value: 3},
				{priority: 5, value: 4},
				{priority: 5, value: 5},
				{priority: 1, value: 0},
			},
			out: []interface{}{0, 1, 2, 3, 4, 5},
		},
	}

	for _, tt := range tests {
		q := New()

		for _, item := range tt.in {
			err := q.Enqueue(item.priority, item.value)
			require.NoError(t, err)
		}

		out := readQueue(q)

		require.Equal(t, tt.out, out)
	}
}

func readQueue(q *PQueue) (values []interface{}) {
	for {
		value, err := q.Dequeue()
		if err != nil {
			break
		}

		values = append(values, value)
	}

	return
}
