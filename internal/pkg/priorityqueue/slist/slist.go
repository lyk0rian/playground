package slist

import (
	"fmt"
)

type Node struct {
	next  *Node
	value interface{}
}

type LinkedList struct {
	head *Node
}

func (l *LinkedList) Traverse() (count int, tail *Node) {
	if l.head == nil {
		return 0, nil
	}

	tail = l.head
	count = 1

	for tail.next != nil {
		tail = tail.next
		count++
	}

	return count, tail
}

func (l *LinkedList) PushLast(data interface{}) error {
	newNode := Node{value: data}
	_, last := l.Traverse()

	if last != nil {
		last.next = &newNode
	} else {
		l.head = &newNode // Head is nil
	}

	return nil
}

func (l *LinkedList) Empty() bool {
	return l.head == nil
}

func (l *LinkedList) PopFirst() (interface{}, error) {
	if l.head == nil {
		return nil, fmt.Errorf("empty list")
	}

	node := l.head
	l.head = node.next

	return node.value, nil
}

func (l *LinkedList) PrintList() {
	if l == nil || l.head == nil {
		fmt.Printf("empty list\n")
		return
	}

	fmt.Printf("list content:\n")
	iter := l.head
	for iter != nil {
		fmt.Printf("%v\n", iter.value)
		iter = iter.next
	}
}
