package slist

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPushLast(t *testing.T) {
	l := LinkedList{}

	l.PushLast(1)
	l.PushLast(2)
	l.PushLast(3)

	require.Equal(t, 1, l.head.value)
	require.Equal(t, 2, l.head.next.value)
	require.Equal(t, 3, l.head.next.next.value)
}

func TestTraverse(t *testing.T) {
	l := LinkedList{}
	c, node := l.Traverse()

	require.Nil(t, node)
	require.Zero(t, c)

	l.PushLast(1)
	l.PushLast(2)
	l.PushLast(3)

	c, node = l.Traverse()

	require.Equal(t, 3, c)
	require.NotNil(t, node)
}

func TestEmpty(t *testing.T) {
	l := LinkedList{}
	require.True(t, l.Empty())

	l.PushLast(1)
	require.False(t, l.Empty())
}

func TestPopFirst(t *testing.T) {
	l := LinkedList{}

	_, err := l.PopFirst()
	require.Error(t, err)

	l.PushLast(1)

	value, err := l.PopFirst()
	require.NoError(t, err)
	require.Equal(t, 1, value)

	require.True(t, l.Empty())

	l.PushLast(2)
	l.PushLast(3)

	value, err = l.PopFirst()
	require.NoError(t, err)
	require.Equal(t, 2, value)

	c, node := l.Traverse()
	require.Equal(t, 1, c)
	require.Equal(t, 3, node.value)
}
