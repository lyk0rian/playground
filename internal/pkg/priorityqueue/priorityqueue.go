package priorityqueue

import (
	"fmt"
	"playground/internal/pkg/priorityqueue/slist"
	"sync"
)

type PQueue struct {
	sync.Mutex
	queue []*slist.LinkedList
}

const MaxPriority = 10

func New() *PQueue {
	return &PQueue{
		queue: make([]*slist.LinkedList, MaxPriority),
	}
}

func (q *PQueue) Name() string {
	return "Приоритетная очередь PriorityQueue"
}

func (q *PQueue) Dir() string {
	return ""
}

func (q *PQueue) Run(params []string) ([]string, error) {
	return nil, nil
}

func checkPriority(priority int) error {
	if priority < 0 {
		return fmt.Errorf("priority is too low")
	}
	if priority > MaxPriority {
		return fmt.Errorf("priority is too high")
	}

	return nil
}

func (q *PQueue) Enqueue(priority int, item interface{}) error {
	q.Lock()
	defer q.Unlock()

	if err := checkPriority(priority); err != nil {
		return err
	}

	if q.queue[priority] == nil {
		q.queue[priority] = &slist.LinkedList{}
	}

	return q.queue[priority].PushLast(item)
}

func (q *PQueue) Dequeue() (interface{}, error) {
	q.Lock()
	defer q.Unlock()

	for priority := 0; priority < MaxPriority; priority++ {
		if q.queue[priority] == nil || q.queue[priority].Empty() {
			// fmt.Printf("skip empty queue for prio %d\n", priority)
			continue
		}

		value, err := q.queue[priority].PopFirst()
		q.queue[priority].PrintList()

		return value, err
	}

	return nil, fmt.Errorf("empty priority queue")
}
