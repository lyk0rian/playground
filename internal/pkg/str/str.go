package str

import (
	"fmt"
	"playground/test"
	"strings"
)

type Str struct {
}

func New() *Str {
	return &Str{}
}

func (ln *Str) Name() string {
	return "String Length"
}
func (ln *Str) Dir() string {
	return test.GetModuleDir(1)
}

func (s *Str) Run(params []string) ([]string, error) {
	if len(params) < 1 {
		return nil, fmt.Errorf("invalid params")
	}
	result := len(strings.Trim(params[0], "\r\n"))

	return []string{fmt.Sprintf("%d", result)}, nil
}
