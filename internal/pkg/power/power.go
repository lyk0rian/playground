package power

import (
	"fmt"
	"playground/test"
	"strconv"
)

type Power struct {
}

func New() *Power {
	return &Power{}
}

func (ln *Power) Name() string {
	return "Итеративный O(N) алгоритм возведения числа в степень"
}

func (ln *Power) Dir() string {
	return test.GetModuleDir(1)
}

func (s *Power) Run(params []string) ([]string, error) {

	a, n, err := s.parseParams(params)
	if err != nil {
		return nil, err
	}

	result := 1.0
	for i := int64(1); i <= n; i++ {
		result = result * a
	}

	return []string{fmt.Sprintf("%f", result)}, nil
}

func (s *Power) parseParams(params []string) (a float64, n int64, err error) {
	if len(params) < 2 {
		err = fmt.Errorf("incorrect params")
		return
	}

	a, err = strconv.ParseFloat(params[0], 64)
	if err != nil {
		return
	}

	n, err = strconv.ParseInt(params[1], 10, 64)

	return
}
