package fib1

import (
	"fmt"
	"math/big"
	"playground/test"
	"strconv"
)

type Fib struct {
}

func New() *Fib {
	return &Fib{}
}

func (f *Fib) Name() string {
	return "Рекурсивный O(2^N) алгоритм поиска чисел Фибоначчи"
}

func (f *Fib) Dir() string {
	return test.GetModuleDir(1)
}

func (f *Fib) Run(params []string) ([]string, error) {
	n, err := f.parseParams(params)
	if err != nil {
		return nil, err
	}

	result := f.fib(n)
	return []string{fmt.Sprintf("%s", result)}, nil
}

func (f *Fib) fib(n int64) (result *big.Int) {
	if n == 1 {
		return big.NewInt(1)
	}

	if n < 1 {
		return big.NewInt(0)
	}

	return new(big.Int).Add(f.fib(n-1), f.fib(n-2))
}

func (f *Fib) parseParams(params []string) (n int64, err error) {
	if len(params) < 1 {
		err = fmt.Errorf("incorrect params")
		return
	}

	n, err = strconv.ParseInt(params[0], 10, 64)
	if err != nil {
		return
	}

	if n < 0 {
		err = fmt.Errorf("incorrect params")
		return
	}
	return
}
