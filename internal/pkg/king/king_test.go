package king

import (
	"playground/test"
	"testing"
)

func TestRun(t *testing.T) {
	test.RunTests(t, New())
}
