package king

import (
	"fmt"
	"playground/test"
	"strconv"
)

type Solver struct {
	primes []int64
}

func New() *Solver {
	return &Solver{}
}

func (s *Solver) Name() string {
	return "Прогулка Короля"
}

func (s *Solver) Dir() string {
	return test.GetModuleDir(1)
}

func (s *Solver) Run(params []string) ([]string, error) {
	cellNo, err := s.parseParams(params)
	if err != nil {
		return nil, err
	}

	mask := getMovesMask(uint64(cellNo))
	moves := popCnt(mask)

	return []string{strconv.FormatUint(uint64(moves), 10), strconv.FormatUint(mask, 10)}, nil
}

func (s *Solver) parseParams(params []string) (n int64, err error) {
	if len(params) < 1 {
		err = fmt.Errorf("incorrect params")
		return
	}

	n, err = strconv.ParseInt(params[0], 10, 64)
	if err != nil {
		return
	}

	if n < 0 || n > 64 {
		err = fmt.Errorf("incorrect params")
		return
	}
	return
}

func getMovesMask(cellNo uint64) (mask uint64) {
	k := uint64(1) << uint64(cellNo)
	noA := uint64(0xfefefefefefefefe)
	noH := uint64(0x7f7f7f7f7f7f7f7f)
	kA := k & noA
	kH := k & noH

	mask = (kA << 7) | (k << 8) | (kH << 9) |
		(kA >> 1) | (kH << 1) |
		(kA >> 9) | (k >> 8) | (kH >> 7)

	return
}

func popCnt(val uint64) int {
	bits := 0
	for val != 0 {
		val &= (val - 1)
		bits++
	}

	return bits
}
