.PHONY: test up build buildw32 run help

test: ## 🎱 Run tests
	@echo "🎱 Starting tests"
	@go test -v ./...

up: ## 🐳 Run tests inside docker
	@echo "🐳 Starting tests in Docker"
	@docker-compose -f deployments/docker-compose.yml up --build --abort-on-container-exit

build: ## 🧱 Build the app for current platform
	@echo "🧱 Building the app"
	@go build -o ./bin/tester cmd/tester/main.go

build\:win: ## 🦄 Build the app for windows platform
	@echo "🦄 Building the app for windows"
	@GOOS=windows GOARCH=amd64 go build -o ./bin/tester.exe cmd/tester/main.go

build\:osx: ## 🍏 Build the app for OSX platform
	@echo "🍏 Building the app for mac"
	@GOOS=darwin GOARCH=amd64 go build -o ./bin/tester-osx cmd/tester/main.go

run: ## 🏃 Run the app
	@echo "🏃 Running the app"
	@go run cmd/tester/main.go

help: ## ❓ Display this help screen
	@grep -h -E '^[a-zA-Z_-][^:]*:.*?## .*$$' $(MAKEFILE_LIST) | \
	sed 's/\\//g' | \
	awk 'BEGIN {FS = ":[^:]*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'