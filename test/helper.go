package test

import (
	"bufio"
	"fmt"
	"os"
	"path"
	"playground/internal/pkg/domain"
	"runtime"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

type TesterOption int

const (
	ShowResult TesterOption = iota
	ShowInput
	RawDuration
	NoCheckResult
	ShowExpected
)

type TestCase struct {
	Name string
	In   []string
	Out  []string
}

func GetTestCases(skip int) (result []TestCase) {
	dir := GetModuleDir(skip)
	return GetTestCasesFromDir(dir)
}

func readFile(fileName string) (data []string, err error) {
	inFile, err := os.Open(fileName)
	if err != nil {
		return
	}

	defer inFile.Close()

	scanner := bufio.NewScanner(inFile)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		data = append(data, strings.Trim(scanner.Text(), "\r\n"))
	}

	return
}

func GetTestCasesFromDir(dir string) (result []TestCase) {
	for i := 0; ; i++ {
		tpl := fmt.Sprintf("%s/tests/test.%d.", dir, i)
		inData, err := readFile(tpl + "in")
		if err != nil {
			break
		}

		outData, err := readFile(tpl + "out")
		if err != nil {
			panic("can't open " + tpl + "out")
		}

		tCase := TestCase{
			Name: fmt.Sprintf("case %d", i),
			In:   inData,
			Out:  outData,
		}

		result = append(result, tCase)
	}

	if len(result) == 0 {
		panic("got 0 tests")
	}

	return
}

func GetModuleDir(skip int) string {
	_, filename, _, ok := runtime.Caller(skip)
	if !ok {
		panic("No caller information")
	}

	return path.Dir(filename)
}

func RunTests(t *testing.T, solver domain.Solver) {
	for _, tt := range GetTestCases(3) {
		t.Run(tt.Name, func(t *testing.T) {
			start := time.Now()
			got, err := solver.Run(tt.In)
			elapsed := time.Since(start)
			require.NoError(t, err)

			t.Logf("test %s took %s", tt.Name, elapsed)

			// got = strings.Trim(got, "\r\n")
			fmt.Printf("expected: %s\ngot: %s\n", tt.Out, got)
			require.Equal(t, tt.Out, got)
		})
	}
}
